// Import only what you need, to help your bundler optimize final code size using tree shaking
// see https://developer.mozilla.org/en-US/docs/Glossary/Tree_shaking)
import {
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
  BoxGeometry,
  Mesh,
  MeshNormalMaterial,
  AmbientLight,
  Clock
} from 'three';

// If you prefer to import the whole library, with the THREE prefix, use the following line instead:
// import * as THREE from 'three'

// NOTE: three/addons alias is supported by Rollup: you can use it interchangeably with three/examples/jsm/  

import {
  OrbitControls
} from 'three/examples/jsm/controls/OrbitControls.js';

import {
  GLTFLoader
} from 'three/examples/jsm/loaders/GLTFLoader.js';

// Example of hard link to official repo for data, if needed
// const MODEL_PATH = 'https://raw.githubusercontent.com/mrdoob/js/r148/examples/models/gltf/LeePerrySmith/LeePerrySmith.glb';


// INSERT CODE HERE


import * as THREE from 'three';
import { ARButton } from 'three/examples/jsm/webxr/ARButton.js';


import { Octree } from 'three/examples/jsm/math/Octree.js';
import { OctreeHelper } from 'three/examples/jsm/helpers/OctreeHelper.js';

import { Capsule } from 'three/examples/jsm/math/Capsule.js';
import { commandOptions } from 'redis';

import { GUI } from 'three/examples/jsm/libs/lil-gui.module.min.js';

let container;
let camera, scene, renderer;
let controller;

let reticle;

let hitTestSource = null;
let hitTestSourceRequested = false;

const clock = new THREE.Clock();

const worldOctree = new Octree();

const keyStates = {};

let fwdValue = 0;
let bkdValue = 0;
let rgtValue = 0;
let lftValue = 0;
let tempVector = new THREE.Vector3();
let upVector = new THREE.Vector3(0, 1, 0);
let joyManager;

let turnLeft = false;
let turnRight = false;

var controls2;

let then = 0;


const playerCollider = new Capsule(new THREE.Vector3(0, 0.35, 0), new THREE.Vector3(0, 1, 0), 0.35);
const GRAVITY = 30;
const playerVelocity = new THREE.Vector3();
const playerDirection = new THREE.Vector3();
let playerOnFloor = false;

document.addEventListener('keydown', (event) => {

  keyStates[event.code] = true;

});

document.addEventListener('keyup', (event) => {

  keyStates[event.code] = false;

});



document.addEventListener('mouseup', () => {

  if (document.pointerLockElement !== null) throwBall();

});

document.body.addEventListener('mousemove', (event) => {

  if (document.pointerLockElement === document.body) {

    camera.rotation.y -= event.movementX / 500;
    camera.rotation.x -= event.movementY / 500;

  }

});

let skinInstance;

const globals = {
  time: 0,
  deltaTime: 0,
};

const manager = new THREE.LoadingManager(
  () => {
    console.log('loaded')
    init();
    animate();
  },

  // Progress
  () => {
    console.log('progress')
  }
);



let models = {
  player: { url: 'assets/models/Soldier.glb', name: 'player' },
  terrain: { url: 'assets/models/inazuma_eleven_terrain_commerce.glb', name: 'terrain' }
};

{
  const gltfLoader = new GLTFLoader(manager);
  for (const model of Object.values(models)) {
    gltfLoader.load(model.url, (gltf) => {
      model.gltf = gltf;

      if (model.name == "terrain") {

        model.gltf

        worldOctree.fromGraphNode(gltf.scene);

        gltf.scene.traverse(child => {

          if (child.isMesh) {

            child.castShadow = true;
            child.receiveShadow = true;

            if (child.material.map) {

              child.material.map.anisotropy = 4;

            }

          }

        });

        const helper = new OctreeHelper(worldOctree);
        helper.visible = false;


        const gui = new GUI({ width: 200 });
        gui.add({ debug: false }, 'debug')
          .onChange(function (value) {

            helper.visible = value;

          });




      }
    });
  }
}

addJoystick();

function addJoystick() {
  const options = {
    zone: document.getElementById("joystickWrapper1"),
    size: 120,
    multitouch: true,
    maxNumberOfNipples: 2,
    mode: "static",
    restJoystick: true,
    shape: "circle",
    // position: { top: 20, left: 20 },
    position: { top: "60px", left: "60px" },
    dynamicPage: true,
  };

  joyManager = nipplejs.create(options);

  joyManager["0"].on("move", function (evt, data) {
    const forward = data.vector.y;
    const turn = data.vector.x;

    if (forward > 0) {
      fwdValue = Math.abs(forward);
      bkdValue = 0;
    } else if (forward < 0) {
      fwdValue = 0;
      bkdValue = Math.abs(forward);
    }

    if (turn > 0) {
      lftValue = 0;
      rgtValue = Math.abs(turn);
    } else if (turn < 0) {
      lftValue = Math.abs(turn);
      rgtValue = 0;
    }
  });

  joyManager["0"].on("end", function (evt) {
    bkdValue = 0;
    fwdValue = 0;
    lftValue = 0;
    rgtValue = 0;
  });
}




function updatePlayer() {
  // move the player
  const angle = controls2.getAzimuthalAngle();
  console.log(`the current azimuth angle is ${angle}`);

  if (fwdValue > 0) {
    tempVector.set(0, 0, -fwdValue).applyAxisAngle(upVector, angle);
    models.player.gltf.scene.position.addScaledVector(tempVector, 1 / 10);
  }

  if (bkdValue > 0) {
    tempVector.set(0, 0, bkdValue).applyAxisAngle(upVector, angle);
    models.player.gltf.scene.position.addScaledVector(tempVector, 1 / 10);
  }

  if (lftValue > 0) {
    tempVector.set(-lftValue, 0, 0).applyAxisAngle(upVector, angle);
    models.player.gltf.scene.position.addScaledVector(tempVector, 1 / 10);
  }

  if (rgtValue > 0) {
    tempVector.set(rgtValue, 0, 0).applyAxisAngle(upVector, angle);
    models.player.gltf.scene.position.addScaledVector(tempVector, 1 / 10);
  }

  models.player.gltf.scene.updateMatrixWorld();

  // controls2.target.set(mesh.position.x, mesh.position.y, mesh.position.z);
  // reposition camera
  camera.position.sub(controls2.target);
  controls2.target.copy(models.player.gltf.scene.position);
  // console.log(mesh.position);
  camera.position.add(models.player.gltf.scene.position.sub(new THREE.Vector3(0, 0, 0)));

  // playerCollider.end = models.player.gltf.scene.position;

}


function controls(deltaTime) {

  var delta = clock.getDelta(); // seconds.
  var moveDistance = 5 * deltaTime; // 200 pixels per second
  var rotateAngle = Math.PI / 2 * deltaTime;   // pi/2 radians (90 degrees) per second
  // gives a bit of air control

  if (keyStates['KeyE']) {

    models.player.gltf.scene.translateZ(-moveDistance);

    //playerVelocity.add(getForwardVector().multiplyScalar(speedDelta));
    //console.log(camera.position)
    // models.player.gltf.scene.position.copy(camera.position)
  }

  if (keyStates['ArrowUp']) {
    models.player.gltf.scene.translateZ(-moveDistance);
  }

  if (keyStates['KeyS']) {

    models.player.gltf.scene.translateZ(moveDistance);
    //playerVelocity.add(getForwardVector().multiplyScalar(- speedDelta));

  }
  if (keyStates['ArrowDown']) {
    models.player.gltf.scene.translateZ(moveDistance);
  }

  if (keyStates['KeyA']) {

    models.player.gltf.scene.translateX(-moveDistance);
    //playerVelocity.add(new THREE.Vector3(0.1, 0.0, -0.1).multiplyScalar(- speedDelta));
    //camera.lookAt(playerDirection)
  }

  if (keyStates['KeyD']) {

    models.player.gltf.scene.translateX(moveDistance);
    //playerVelocity.add(new THREE.Vector3(0.1, 0.0, 0.1).multiplyScalar(speedDelta));

  }

  //console.log(keyStates)

  //rotate left/right/up/down
  var rotation_matrix = new THREE.Matrix4().identity();
  if (keyStates['ArrowLeft']) {
    models.player.gltf.scene.rotateOnAxis(new THREE.Vector3(0, 1, 0), rotateAngle);
  }

  if (turnLeft) {
    models.player.gltf.scene.rotateOnAxis(new THREE.Vector3(0, 1, 0), rotateAngle);
  }

  if (keyStates['ArrowRight']) {
    models.player.gltf.scene.rotateOnAxis(new THREE.Vector3(0, 1, 0), -rotateAngle);
  }

  if (turnRight) {
    models.player.gltf.scene.rotateOnAxis(new THREE.Vector3(0, 1, 0), -rotateAngle);
  }

  turnRight = false;
  turnLeft = false;
  var relativeCameraOffset = new THREE.Vector3(0, 2, 4);


}

function playerCollisions() {

  const result = worldOctree.capsuleIntersect(playerCollider);

  playerOnFloor = false;

  if (result) {




    playerOnFloor = result.normal.y > 0;



    if (!playerOnFloor) {

      console.log("collision")
      playerVelocity.addScaledVector(result.normal, - result.normal.dot(playerVelocity));

    }

    playerCollider.translate(result.normal.multiplyScalar(result.depth));

  }

}

function updatePlayerCollision(deltaTime) {


  // console.log(playerCollider.start)
  let damping = Math.exp(- 4 * deltaTime) - 1;

  if (!playerOnFloor) {

    playerVelocity.y -= GRAVITY * deltaTime;

    // small air resistance
    damping *= 0.1;

  }

  playerVelocity.addScaledVector(playerVelocity, damping);

  const deltaPosition = playerVelocity.clone().multiplyScalar(deltaTime);
  //console.log(deltaPosition)
  playerCollider.translate(deltaPosition);

  playerCollisions();

  // models.player.gltf.scene.position.copy(playerCollider.end);

}


function init() {

  container = document.createElement('div');
  document.body.appendChild(container);

  scene = new THREE.Scene();

  camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.01, 20);

  const light = new THREE.HemisphereLight(0xffffff, 0xbbbbff, 1);
  light.position.set(0.5, 1, 0.25);
  scene.add(light);

  //

  renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.xr.enabled = true;
  container.appendChild(renderer.domElement);

  controls2 = new OrbitControls(camera, renderer.domElement);
  controls2.maxDistance = 100;
  controls2.minDistance = 100;
  //controls2.maxPolarAngle = (Math.PI / 4) * 3;
  controls2.maxPolarAngle = Math.PI / 2;
  controls2.minPolarAngle = 0;
  controls2.autoRotate = false;
  controls2.autoRotateSpeed = 0;
  controls2.rotateSpeed = 0.4;
  controls2.enableDamping = false;
  controls2.dampingFactor = 0.1;
  controls2.enableZoom = false;
  controls2.enablePan = false;
  controls2.minAzimuthAngle = -Math.PI / 2; // radians
  controls2.maxAzimuthAngle = Math.PI / 4; // radians

  //
  let options = { requiredFeatures: ['hit-test'], optionalFeatures: ["dom-overlay"] };
  options.domOverlay = { root: document.getElementById("contentAr") }


  document.body.appendChild(ARButton.createButton(renderer, options));

  function onSelect() {

    if (reticle.visible) {

      for (const model of Object.values(models)) {
        let gltf = model.gltf;
        gltf.scene.scale.y = 0.2;
        gltf.scene.scale.z = 0.2;
        gltf.scene.scale.x = 0.2;

        // gltf.scene.position.set(0, 0.1, 0)

        console.log(gltf.scene.position)


        reticle.matrix.decompose(gltf.scene.position, gltf.scene.quaternion, gltf.scene.scale);
        scene.add(model.gltf.scene)

        prepModelsAndAnimations()
        skinInstance = new SkinInstance(models.player);
        skinInstance.setAnimation('Run');


        ;
      }



    }

  }

  class SkinInstance {
    constructor(model) {
      this.model = model;

      this.skeleton = new THREE.SkeletonHelper(this.model.gltf.scene);
      this.skeleton.visible = true;
      scene.add(this.skeleton);


      //this.animRoot = SkeletonUtils.clone(this.model.gltf.scene);
      //this.model.gltf.scene.add(this.animRoot)
      this.mixer = new THREE.AnimationMixer(this.model.gltf.scene);
      this.actions = {};
    }
    setAnimation(animName) {
      const clip = this.model.animations[animName];
      // turn off all current actions
      for (const action of Object.values(this.actions)) {
        action.enabled = false;
      }
      // get or create existing action for clip
      const action = this.mixer.clipAction(clip);
      action.enabled = true;

      action.setEffectiveTimeScale(1);
      action.setEffectiveWeight(100);

      action.reset();
      action.play();
      this.actions[animName] = action;
      console.log(this.actions)
    }
    update() {
      this.mixer.update(globals.deltaTime);
    }
  }



  function prepModelsAndAnimations() {
    //console.log(models)
    Object.values(models).forEach(model => {
      if (model.name == "player") {
        const animsByName = {};
        model.gltf.animations.forEach((clip) => {
          animsByName[clip.name] = clip;
          // Should really fix this in .blend file
          if (clip.name === 'Walk') {
            clip.duration /= 2;
          }
        });
        model.animations = animsByName;

      }

    });
  }

  document.getElementById("turnRight").addEventListener("click", function () { turnRight = true; console.log("right") })
  document.getElementById("turnLeft").addEventListener("click", function () { turnLeft = true; console.log("left") })

  controller = renderer.xr.getController(0);
  controller.addEventListener('select', onSelect);
  scene.add(controller);

  reticle = new THREE.Mesh(
    new THREE.RingGeometry(0.15, 0.2, 32).rotateX(- Math.PI / 2),
    new THREE.MeshBasicMaterial()
  );
  reticle.matrixAutoUpdate = false;
  reticle.visible = false;
  scene.add(reticle);
  //

  window.addEventListener('resize', onWindowResize);

}



function onWindowResize() {

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);

}

//

function animate() {

  renderer.setAnimationLoop(render);

}

function render(timestamp, frame) {

  // document.getElementById("place").style.display = "block";




  globals.time = timestamp * 0.001;
  // make sure delta time isn't too big.
  globals.deltaTime = Math.min(globals.time - then, 1 / 20);
  then = globals.time;

  if (skinInstance != null) {

    //console.log("walk");
    skinInstance.update();

    controls(globals.deltaTime);
    updatePlayerCollision(globals.deltaTime);
    updatePlayer();

  }


  if (frame) {

    const referenceSpace = renderer.xr.getReferenceSpace();
    const session = renderer.xr.getSession();

    if (hitTestSourceRequested === false) {

      session.requestReferenceSpace('viewer').then(function (referenceSpace) {

        session.requestHitTestSource({ space: referenceSpace }).then(function (source) {

          hitTestSource = source;

        });

      });

      session.addEventListener('end', function () {

        hitTestSourceRequested = false;
        hitTestSource = null;

      });

      hitTestSourceRequested = true;

    }

    if (hitTestSource) {

      const hitTestResults = frame.getHitTestResults(hitTestSource);

      if (hitTestResults.length) {


        const hit = hitTestResults[0];

        reticle.visible = true;
        reticle.matrix.fromArray(hit.getPose(referenceSpace).transform.matrix);

      } else {

        reticle.visible = false;


      }

    }

  }

  renderer.render(scene, camera);

}
